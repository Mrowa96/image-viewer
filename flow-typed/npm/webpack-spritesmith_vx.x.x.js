// flow-typed signature: 8966a1e89107dd76b12bc153fd37e1a6
// flow-typed version: <<STUB>>/webpack-spritesmith_v^1.0.0/flow_v0.92.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'webpack-spritesmith'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'webpack-spritesmith' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'webpack-spritesmith/lib/compileNormal' {
  declare module.exports: any;
}

declare module 'webpack-spritesmith/lib/compileRetina' {
  declare module.exports: any;
}

declare module 'webpack-spritesmith/lib/Plugin' {
  declare module.exports: any;
}

declare module 'webpack-spritesmith/lib/processOptions' {
  declare module.exports: any;
}

declare module 'webpack-spritesmith/lib/substitute' {
  declare module.exports: any;
}

declare module 'webpack-spritesmith/lib/utils' {
  declare module.exports: any;
}

declare module 'webpack-spritesmith/lib/writeCss' {
  declare module.exports: any;
}

// Filename aliases
declare module 'webpack-spritesmith/lib/compileNormal.js' {
  declare module.exports: $Exports<'webpack-spritesmith/lib/compileNormal'>;
}
declare module 'webpack-spritesmith/lib/compileRetina.js' {
  declare module.exports: $Exports<'webpack-spritesmith/lib/compileRetina'>;
}
declare module 'webpack-spritesmith/lib/Plugin.js' {
  declare module.exports: $Exports<'webpack-spritesmith/lib/Plugin'>;
}
declare module 'webpack-spritesmith/lib/processOptions.js' {
  declare module.exports: $Exports<'webpack-spritesmith/lib/processOptions'>;
}
declare module 'webpack-spritesmith/lib/substitute.js' {
  declare module.exports: $Exports<'webpack-spritesmith/lib/substitute'>;
}
declare module 'webpack-spritesmith/lib/utils.js' {
  declare module.exports: $Exports<'webpack-spritesmith/lib/utils'>;
}
declare module 'webpack-spritesmith/lib/writeCss.js' {
  declare module.exports: $Exports<'webpack-spritesmith/lib/writeCss'>;
}
