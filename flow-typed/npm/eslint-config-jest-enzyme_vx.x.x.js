// flow-typed signature: 51ed52bafb8e1785fdac65519e81a758
// flow-typed version: <<STUB>>/eslint-config-jest-enzyme_v^7.0.1/flow_v0.92.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'eslint-config-jest-enzyme'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'eslint-config-jest-enzyme' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */


// Filename aliases
declare module 'eslint-config-jest-enzyme/index' {
  declare module.exports: $Exports<'eslint-config-jest-enzyme'>;
}
declare module 'eslint-config-jest-enzyme/index.js' {
  declare module.exports: $Exports<'eslint-config-jest-enzyme'>;
}
