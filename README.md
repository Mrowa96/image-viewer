# Image viewer

### Description

Simple image viewer based on React

### Additional info:

React, ract-dom and prettier should have exact version of packages
If flow throws error about missing package run `./node_modules/.bin/flow-typed install`
Tests have to have suffix `.test` in order to eslint lint them correctly (eslint-plugin-enzyme foult)

### How to run locally

- `npm i`
- `npm start`

### TODO

- image placeholders
- rewrite image list to grid
- move images outside
- add production server
- fix flow error in react-icons dependency
