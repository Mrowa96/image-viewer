export type Params = {
  id: string,
};

export type Match = {
  params: Params,
};

export type History = {
  push: Function,
};

export type RouteProp = {
  match: Match,
  history: History,
};
