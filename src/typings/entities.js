export type Image = {
  id: number,
  name: string,
  thumbUrl: string,
  url: string,
};
