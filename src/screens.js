import React, { type Element } from 'react';
import ImageList from './containers/ImageList';
import ImageDetail from './containers/ImageDetail';
import NotFound from './components/NotFound';

export const HomePage: Function = (): Element<typeof ImageList> => (
  <ImageList />
);

export const ImageDetailPage: Function = (): Element<typeof ImageDetail> => (
  <ImageDetail />
);

export const NotFoundPage: Function = (): Element<typeof NotFound> => (
  <NotFound />
);
