import './index.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { BrowserRouter } from 'react-router-dom';
import reducers from './state/reducers';
import initialState from './state/store';
import App from './components/App';

const app: HTMLElement | null = document.getElementById('app');
// TODO Add typings
const store: any = createStore(reducers, initialState);

if (!app) {
  throw new Error('App container does not exists!');
}

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  app,
);
