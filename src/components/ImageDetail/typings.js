import { type Image } from '../../typings/entities';
import { type RouteProp } from '../../typings/routes';

export type ImageDetailProps = RouteProp & {
  image: Image | typeof undefined,
};
