import type { Image } from '../../../../typings/entities';

export type ToolbarProps = {
  image: Image,
  renameImage: Function,
  removeImage: Function,
};
