import React, { type Element, useState } from 'react';
import {
  IoIosClose,
  IoIosColorWand,
  IoIosSave,
  IoIosTrash,
} from 'react-icons/io';
import IconButton from '../../../IconButton';
import styles from './styles.css';
import type { ToolbarProps } from './typings';

const Toolbar: Function = ({
  image,
  renameImage,
  removeImage,
}: ToolbarProps): Element<any> => {
  const [newName, setNewName] = useState(null);
  const [editMode, setEditMode] = useState(false);

  const onInputNameChange: Function = (
    event: SyntheticEvent<HTMLInputElement>,
  ): void => {
    setNewName(event?.currentTarget?.value);
  };
  const onRemoveButtonClick: Function = (): void => {
    removeImage(image?.id);
  };
  const onSaveButtonClick: Function = (): void => {
    if (newName && newName.length > 3) {
      renameImage(image?.id, newName);
      setEditMode(false);
    }
  };
  const onDiscardButtonClick: Function = (): void => {
    setNewName(null);
    setEditMode(false);
  };
  const onEditButtonClick: Function = (): void => {
    setNewName(null);
    setEditMode(true);
  };

  return (
    <div className={styles.Toolbox}>
      <div className={styles.Editor}>
        {!editMode && (
          <>
            <span className={styles.Name}>{image.name}</span>

            <IconButton onClick={onEditButtonClick} label="Edit image name">
              <IoIosColorWand />
            </IconButton>
          </>
        )}
        {editMode && (
          <>
            <input
              type="text"
              className={styles.Name}
              onChange={onInputNameChange}
              onInput={onInputNameChange}
              value={newName === null ? image.name : newName}
            />

            {newName !== null && newName.length <= 3 && (
              <span className={styles.ErrorMessage}>
                Image name is too short
              </span>
            )}

            <IconButton
              onClick={onSaveButtonClick}
              label="Save new image name"
              additionalClass={styles.SaveButton}>
              <IoIosSave />
            </IconButton>

            <IconButton onClick={onDiscardButtonClick} label="Discard changes">
              <IoIosClose />
            </IconButton>
          </>
        )}
      </div>
      <IconButton
        onClick={onRemoveButtonClick}
        label="Remove image"
        additionalClass={styles.RemoveButton}>
        <IoIosTrash />
      </IconButton>
    </div>
  );
};

export default Toolbar;
