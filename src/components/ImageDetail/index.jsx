import React, { type Element, PureComponent } from 'react';
import NotFound from '../NotFound';
import Toolbar from '../../containers/ImageDetail/components/Toolbar';
import styles from './styles.css';
import { type ImageDetailProps } from './typings';

export default class ImageDetail extends PureComponent<ImageDetailProps> {
  componentDidUpdate(prevProps: ImageDetailProps): void {
    const { image, history }: ImageDetailProps = this.props;

    if (!image && prevProps.image) {
      history.push('/');
    }
  }

  render(): Element<any> {
    const { image }: ImageDetailProps = this.props;

    if (!image) {
      return <NotFound />;
    }

    return (
      <>
        <div className={styles.Wrapper}>
          <div className={styles.ImageWrapper}>
            <picture className={styles.ImageInnerWrapper}>
              <source
                srcSet={`${image.url}.webp`}
                type="image/webp"
                className={styles.Image}
              />
              <img
                src={`${image.url}.jpg`}
                alt={image.name}
                className={styles.Image}
              />
            </picture>
          </div>

          <Toolbar image={image} />
        </div>
      </>
    );
  }
}
