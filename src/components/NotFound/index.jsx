import React, { type Element } from 'react';
import { Link } from 'react-router-dom';
import styles from './styles.css';

const NotFound: Function = (): Element<any> => (
  <div className={styles.Message}>
    <p>Page was not found</p>
    <Link to="/">Go to homepage</Link>
  </div>
);

export default NotFound;
