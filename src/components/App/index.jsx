import React from 'react';
import { Link, Route, Switch } from 'react-router-dom';
import routes from '../../routes';
import styles from './styles.css';

const App = () => (
  <>
    <header className={styles.Header}>
      <Link to="/" className={styles.Link}>
        Image Viewer
      </Link>
    </header>

    <section className={styles.Content}>
      <Switch>
        {routes.map(({ name, path, exact, component }) => (
          <Route
            key={`route-${name}`}
            path={path}
            exact={exact}
            component={component}
          />
        ))}
      </Switch>
    </section>
  </>
);

export default App;
