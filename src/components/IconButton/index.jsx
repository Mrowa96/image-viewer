import React, { type Element } from 'react';
import classnames from 'classnames';
import styles from './styles.css';
import type { IconButtonProps } from './typings';

const IconButton: Function = ({
  label,
  onClick,
  children,
  additionalClass = '',
}: IconButtonProps): Element<any> => (
  <button
    type="button"
    className={classnames(styles.IconButton, {
      [additionalClass]: !!additionalClass,
    })}
    onClick={onClick}
    title={label}>
    {children}
  </button>
);

IconButton.defaultProps = {
  onClick: undefined,
};

export default IconButton;
