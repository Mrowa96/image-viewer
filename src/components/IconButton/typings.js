import * as React from 'react';

export type IconButtonProps = {
  label: string,
  onClick?: Function,
  additionalClass?: string,
  children: React.Node,
};
