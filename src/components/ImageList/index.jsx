import React, { type Element } from 'react';
import ImageOnList from './components/ImageOnList';
import styles from './styles.css';
import { type ImageListProps } from './typings';

const ImageList: Function = ({ images }: ImageListProps): Element<any> => {
  return (
    <>
      {images.length > 0 && (
        <div className={styles.Wrapper}>
          {images.map(image => (
            <ImageOnList key={`image-${image.id}`} image={image} />
          ))}
        </div>
      )}
      {!images.length && (
        <span className={styles.NoImagesMessage}>No images to display</span>
      )}
    </>
  );
};

export default ImageList;
