import { type Image } from '../../typings/entities';

export type ImageListProps = {
  images: Array<Image>,
};
