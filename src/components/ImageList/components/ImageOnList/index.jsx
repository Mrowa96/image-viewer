import React, { type Element } from 'react';
import { Link } from 'react-router-dom';
import styles from './styles.css';
import { type ImageOnListProps } from './typings';

const ImageOnList: Function = ({
  image: { id, thumbUrl, name },
}: ImageOnListProps): Element<any> => (
  <Link to={`/image/${id}`} className={styles.Wrapper}>
    <figure className={styles.InnerWrapper}>
      <picture>
        <source
          srcSet={`${thumbUrl}.webp`}
          type="image/webp"
          className={styles.Image}
        />
        <img src={`${thumbUrl}.jpg`} alt={name} className={styles.Image} />
      </picture>
      <figcaption className={styles.Name}>{name}</figcaption>
    </figure>
  </Link>
);

export default ImageOnList;
