import type { Image } from '../../../../typings/entities';

export type ImageOnListProps = {
  image: Image,
};
