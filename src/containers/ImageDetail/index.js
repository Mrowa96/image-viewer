import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ImageDetail from '../../components/ImageDetail';
import type { InitialState } from '../../state/store/typings';
import type { RouteProp } from '../../typings/routes';

const mapStateToProps: Function = (
  { images }: InitialState,
  {
    match: {
      params: { id },
    },
  }: RouteProp,
) => ({ image: images.find(image => image.id === +id) });

export default withRouter(connect(mapStateToProps)(ImageDetail));
