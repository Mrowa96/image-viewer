import { connect } from 'react-redux';
import { renameImage, removeImage } from '../../../../state/actions/images';
import Toolbar from '../../../../components/ImageDetail/components/Toolbar';

const mapDispatchToProps: Function = (dispatch: Function) => ({
  renameImage: (imageId: number, newName: string): void =>
    dispatch(renameImage(imageId, newName)),
  removeImage: (imageId: number): void => dispatch(removeImage(imageId)),
});

export default connect(
  null,
  mapDispatchToProps,
)(Toolbar);
