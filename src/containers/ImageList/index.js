import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ImageList from '../../components/ImageList';
import type { ImageListProps } from '../../components/ImageList/typings';
import type { InitialState } from '../../state/store/typings';

const mapStateToProps = ({ images }: InitialState): ImageListProps => ({
  images,
});

export default withRouter(
  connect(
    mapStateToProps,
    null,
  )(ImageList),
);
