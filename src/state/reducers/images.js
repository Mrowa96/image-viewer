import { RENAME_IMAGE, REMOVE_IMAGE } from '../actions/images';
import initialState from '../store';
import type { Action } from '../../typings/actions';
import type { Image } from '../../typings/entities';

const images = (
  state: Array<Image> = initialState.images,
  { type, payload }: Action,
): Array<Image> => {
  switch (type) {
    case RENAME_IMAGE:
      return state.map(
        (image: Image): Image => {
          if (image.id === payload.imageId) {
            return { ...image, name: payload.newName };
          }

          return image;
        },
      );

    case REMOVE_IMAGE:
      return state.filter(
        (image: Image): boolean => image.id !== payload.imageId,
      );

    default:
      return state;
  }
};

export default images;
