import { combineReducers } from 'redux';
import images from './images';

const reducers: Function = combineReducers({
  images,
});

export default reducers;
