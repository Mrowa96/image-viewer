import type { Image } from '../../typings/entities';

export type InitialState = {
  images: Array<Image>,
};
