const initialState = {
  images: [
    {
      id: 3325,
      name: 'Test name',
      thumbUrl: '/images/3325_thumb',
      url: '/images/3325',
    },
    {
      id: 3328,
      name: 'Test name',
      thumbUrl: '/images/3328_thumb',
      url: '/images/3328',
    },
    {
      id: 4051,
      name: 'Test name',
      thumbUrl: '/images/4051_thumb',
      url: '/images/4051',
    },
    {
      id: 4059,
      name: 'Test name',
      thumbUrl: '/images/4059_thumb',
      url: '/images/4059',
    },
    {
      id: 4078,
      name: 'Test name',
      thumbUrl: '/images/4078_thumb',
      url: '/images/4078',
    },
    {
      id: 4081,
      name: 'Test name',
      thumbUrl: '/images/4081_thumb',
      url: '/images/4081',
    },
    {
      id: 4087,
      name: 'Test name',
      thumbUrl: '/images/4087_thumb',
      url: '/images/4087',
    },
    {
      id: 4105,
      name: 'Test name',
      thumbUrl: '/images/4105_thumb',
      url: '/images/4105',
    },
    {
      id: 4109,
      name: 'Test name',
      thumbUrl: '/images/4109_thumb',
      url: '/images/4109',
    },
    {
      id: 4112,
      name: 'Test name',
      thumbUrl: '/images/4112_thumb',
      url: '/images/4112',
    },
    {
      id: 4117,
      name: 'Test name',
      thumbUrl: '/images/4117_thumb',
      url: '/images/4117',
    },
    {
      id: 4123,
      name: 'Test name',
      thumbUrl: '/images/4123_thumb',
      url: '/images/4123',
    },
    {
      id: 4125,
      name: 'Test name',
      thumbUrl: '/images/4125_thumb',
      url: '/images/4125',
    },
    {
      id: 4127,
      name: 'Test name',
      thumbUrl: '/images/4127_thumb',
      url: '/images/4127',
    },
    {
      id: 4136,
      name: 'Test name',
      thumbUrl: '/images/4136_thumb',
      url: '/images/4136',
    },
    {
      id: 4158,
      name: 'Test name',
      thumbUrl: '/images/4158_thumb',
      url: '/images/4158',
    },
    {
      id: 4189,
      name: 'Test name',
      thumbUrl: '/images/4189_thumb',
      url: '/images/4189',
    },
    {
      id: 4194,
      name: 'Test name',
      thumbUrl: '/images/4194_thumb',
      url: '/images/4194',
    },
    {
      id: 4200,
      name: 'Test name',
      thumbUrl: '/images/4200_thumb',
      url: '/images/4200',
    },
  ],
};

export default initialState;
