import type { RenameImage, RemoveImage } from './typings';

export const RENAME_IMAGE: string = 'RENAME_IMAGE';
export const REMOVE_IMAGE: string = 'REMOVE_IMAGE';

export const renameImage: Function = (
  imageId: number,
  newName: string,
): RenameImage => {
  return {
    type: RENAME_IMAGE,
    payload: {
      imageId,
      newName,
    },
  };
};

export const removeImage: Function = (imageId: number): RemoveImage => {
  return {
    type: REMOVE_IMAGE,
    payload: {
      imageId,
    },
  };
};
