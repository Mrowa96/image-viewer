import type { Action } from '../../typings/actions';

export type RenameImage = Action & {
  payload: {
    imageId: number,
    newName: string,
  },
};

export type RemoveImage = Action & {
  payload: {
    imageId: number,
  },
};
