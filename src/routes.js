import { HomePage, ImageDetailPage, NotFoundPage } from './screens';

const routes = [
  {
    name: 'home',
    path: '/',
    component: HomePage,
    exact: true,
  },
  {
    name: 'image-detail',
    path: '/image/:id',
    component: ImageDetailPage,
    exact: true,
  },
  {
    name: 'notFound',
    path: undefined,
    component: NotFoundPage,
    exact: false,
  },
];

export default routes;
