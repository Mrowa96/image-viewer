import reducers from '../../../src/state/reducers/index';

describe('images reducers', () => {
  it('should handle RENAME_IMAGE action', () => {
    const state = reducers(
      {
        images: [
          {
            id: 12344,
            name: 'Old name 1',
          },
          {
            id: 12345,
            name: 'Old name 2',
          },
          {
            id: 12346,
            name: 'Old name 3',
          },
        ],
      },
      {
        type: 'RENAME_IMAGE',
        payload: {
          imageId: 12345,
          newName: 'New name',
        },
      },
    );
    const expectedState = [
      {
        id: 12344,
        name: 'Old name 1',
      },
      {
        id: 12345,
        name: 'New name',
      },
      {
        id: 12346,
        name: 'Old name 3',
      },
    ];

    expect(state.images).toEqual(expectedState);
  });

  it('should handle REMOVE_IMAGE action', () => {
    const state = reducers(
      {
        images: [
          {
            id: 12344,
            name: 'Old name 1',
          },
          {
            id: 12345,
            name: 'Old name 2',
          },
          {
            id: 12346,
            name: 'Old name 3',
          },
        ],
      },
      {
        type: 'REMOVE_IMAGE',
        payload: {
          imageId: 12345,
        },
      },
    );
    const expectedState = [
      {
        id: 12344,
        name: 'Old name 1',
      },
      {
        id: 12346,
        name: 'Old name 3',
      },
    ];

    expect(state.images).toEqual(expectedState);
  });
});
