import { renameImage, removeImage } from '../../../src/state/actions/images';

describe('images actions', () => {
  it('should create an action for RENAME_IMAGE', () => {
    const expectedAction = {
      type: 'RENAME_IMAGE',
      payload: {
        imageId: 12345,
        newName: 'Test name',
      },
    };

    expect(renameImage(12345, 'Test name')).toEqual(expectedAction);
  });

  it('should create an action for REMOVE_IMAGE', () => {
    const expectedAction = {
      type: 'REMOVE_IMAGE',
      payload: {
        imageId: 12345,
      },
    };

    expect(removeImage(12345)).toEqual(expectedAction);
  });
});
