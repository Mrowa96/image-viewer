import IconButton from '../../../src/components/IconButton';

describe('Button component', () => {
  let component;
  let onClick;

  beforeEach(() => {
    onClick = jest.fn();
    component = shallow(
      <IconButton label="Test label" onClick={onClick}>
        <span>Test</span>
      </IconButton>,
    );
  });

  it('should render button with text', () => {
    expect(component.text()).toEqual('Test');
  });

  it('should render button with title', () => {
    expect(component.find('button').prop('title')).toEqual('Test label');
  });

  it('should trigger onClick method', () => {
    component.simulate('click');

    expect(onClick.mock.calls.length).toEqual(1);
  });

  it('should add additional class', () => {
    component.setProps({ additionalClass: 'testClass' });

    expect(component.find('button').hasClass('testClass')).toBeTruthy();
  });
});
