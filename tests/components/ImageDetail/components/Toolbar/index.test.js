import Toolbar from '../../../../../src/components/ImageDetail/components/Toolbar';

describe('Toolbar component', () => {
  let component;
  let renameImage;
  let removeImage;
  const image = {
    id: 123,
    thumbUrl: '/thumb',
    url: '/full-url',
    name: 'Test',
  };

  beforeEach(() => {
    renameImage = jest.fn();
    removeImage = jest.fn();
    component = shallow(
      <Toolbar
        image={image}
        renameImage={renameImage}
        removeImage={removeImage}
      />,
    );
  });

  it('should render element with image name', () => {
    expect(component.find('.Name').text()).toEqual('Test');
  });

  it('should render button which will enable edit mode', () => {
    expect(component.find('.Name + IconButton').length).toEqual(1);
  });

  it('should render button which will remove image after click', () => {
    const button = component.find('.Toolbox > IconButton');

    expect(button.length).toEqual(1);

    button.simulate('click');

    expect(removeImage.mock.calls.length).toEqual(1);
  });
});
