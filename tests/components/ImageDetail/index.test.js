import ImageDetail from '../../../src/components/ImageDetail';

describe('ImageDetail component', () => {
  let history;
  let component;
  const image = {
    id: 123,
    thumbUrl: '/thumb',
    url: '/full-url',
    name: 'Test',
  };
  beforeEach(() => {
    history = { push: jest.fn() };
    component = shallow(<ImageDetail image={image} history={history} />);
  });

  it('should render NotFound component if image is undefined', () => {
    component.setProps({ image: undefined });
    expect(component.find('NotFound').length).toEqual(1);
  });

  it('should render webp image with jpg fallback', () => {
    expect(component.find('picture > source').prop('srcSet')).toEqual(
      '/full-url.webp',
    );
    expect(component.find('picture > img').prop('src')).toEqual(
      '/full-url.jpg',
    );
  });

  it('should call push function from history when image change to undefined', () => {
    component.setProps({ image: undefined });

    expect(history.push.mock.calls.length).toEqual(1);
  });
});
