import NotFound from '../../../src/components/NotFound';

describe('NotFound component', () => {
  let component;

  beforeEach(() => {
    component = shallow(<NotFound />);
  });

  it('should render message with text', () => {
    expect(component.find('.Message p').text()).toEqual('Page was not found');
  });

  it('should render link to homepage', () => {
    const link = component.find('Link');

    expect(link.prop('to')).toEqual('/');
  });
});
