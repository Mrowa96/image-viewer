import ImageList from '../../../src/components/ImageList';

describe('ImageList component', () => {
  let component;

  it('should render message with text', () => {
    component = shallow(<ImageList images={[]} />);

    expect(component.text()).toEqual('No images to display');
  });

  it('should render link to homepage', () => {
    component = shallow(<ImageList images={[{}, {}]} />);

    expect(component.find('ImageOnList').length).toEqual(2);
  });
});
