import ImageOnList from '../../../../../src/components/ImageList/components/ImageOnList';

describe('ImageOnList component', () => {
  let component;

  beforeEach(() => {
    const context = {
      router: {
        history: { createHref: jest.fn(), push: jest.fn(), replace: jest.fn() },
      },
    };
    const image = {
      id: 123,
      thumbUrl: '/thumb',
      url: '/full-url',
      name: 'Test',
    };
    component = shallow(<ImageOnList image={image} />, { context })
      .find('Link')
      .dive();
  });

  it('should render figure', () => {
    expect(component.find('figure').length).toEqual(1);
  });

  it('should render webp thumbnail with jpg fallback', () => {
    expect(component.find('picture > source').prop('srcSet')).toEqual(
      '/thumb.webp',
    );
    expect(component.find('picture > img').prop('src')).toEqual('/thumb.jpg');
  });

  it('should render image name in figcaption', () => {
    expect(component.find('figcaption').text()).toEqual('Test');
  });
});
